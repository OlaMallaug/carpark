﻿using CarPark.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarPark.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if(TempData.Count > 0)
                ViewBag.Message = TempData["Message"].ToString();

            return View();
        }

        public ActionResult List()
        {
            var carList = new DatabaseModel().GetAllCars();

            return View(carList);
        }

        public ActionResult Details(int id = 0)
        {
            var car = new DatabaseModel().Find(id);

            return View(car);
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(Car car)
        {
            int result = new DatabaseModel().Register(car);
            
            if(result == -1)
                TempData["Message"] = "Vehicle Registered";
            else
                TempData["Message"] = "Vehicle Registered Failed";

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Modify(int Id = 0)
        {
            var car = new DatabaseModel().Find(Id);

            return View(car);
        }

        [HttpPost]
        public ActionResult Modify(Car car)
        {
            int result = new DatabaseModel().Modify(car);

            if(result == -1)
                TempData["Message"] = "Vehicle Modified";
            else
                TempData["Message"] = "Vehicle Modification Failed";

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Search()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(Car car)
        {
            var result = new DatabaseModel().Search(car.VRegNo);

            if (result == 0)
            {
                ViewBag.ErrorMessage = "No Vehicles Found";
                return View();
            }
            else
                return RedirectToAction("Details", "Home", new { id = result });
        }

        public ActionResult Delete(Car car)
        {
            int result = new DatabaseModel().Delete(car);

            if(result == -1)
                TempData["Message"] = "Vehicle Deleted";
            else
                TempData["Message"] = "Vehicle Deletion Failed";
            return RedirectToAction("Index", "Home");
        }

    }
}