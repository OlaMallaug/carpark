﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarPark.Models
{
    public class Car
    {
        public string ID { get; set; }
        [DisplayName("First Name")]
        public string OFirstName { get; set; }
        [DisplayName("Last Name")]
        public string OLastName { get; set; }
        [DisplayName("Phone Number")]
        public string OPhone { get; set; }
        [DisplayName("Unit No.")]
        public string OUnit { get; set; }
        [DisplayName("Apartment No.")]
        public string OAppNo { get; set; }
        [DisplayName("Vehicle Make")]
        public string VMake{ get; set; }
        [DisplayName("Vehicle Model")]
        public string VModel { get; set; }
        [DisplayName("Vehicle Reg. No.")]
        public string VRegNo { get; set; }
        [DisplayName("Registered")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime VRegDate { get; set; }
    }
}