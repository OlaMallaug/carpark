﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarPark.Models
{
    public class DatabaseModel
    {
        string connectionString = ConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString;

        public Car Find(int id)
        {
            Car car = new Car();

            try
            {
                string sql = "select * from CarPark where ID=@Id";

                using (SqlConnection connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@Id", id);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Read();
                    car.ID = reader["ID"].ToString();
                    car.OFirstName = reader["OFirstName"].ToString();
                    car.OLastName = reader["OLastName"].ToString();
                    car.OPhone = reader["OPhone"].ToString();
                    car.OUnit = reader["OUnit"].ToString();
                    car.OAppNo = reader["OAppNo"].ToString();
                    car.VMake = reader["VMake"].ToString();
                    car.VModel = reader["VModel"].ToString();
                    car.VRegNo = reader["VRegNo"].ToString();
                    car.VRegDate = Convert.ToDateTime(reader["VRegDate"]);

                    connection.Close();
                }
            }
            catch
            {

            }

            return car;
        }

        public int Search(string registration)
        {
            int carID = 0;

            try
            {
                string sql = "select * from CarPark where VRegNo=@RegNo";

                using (SqlConnection connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@RegNo", registration);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Read();
                    carID = Convert.ToInt32(reader["ID"]);

                    connection.Close();
                }
            }
            catch(Exception ex)
            {
                carID = 0;
            }

           
            return carID;
        }

        public List<Car> GetAllCars()
        {
            List<Car> cars = new List<Car>();

            try
            {
                string sql = "select * from CarPark";

                using (SqlConnection connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var car = new Car();
                        car.ID = reader["ID"].ToString();
                        car.OFirstName = reader["OFirstName"].ToString();
                        car.OLastName = reader["OLastName"].ToString();
                        car.OPhone = reader["OPhone"].ToString();
                        car.OUnit = reader["OUnit"].ToString();
                        car.OAppNo = reader["OAppNo"].ToString();
                        car.VMake = reader["VMake"].ToString();
                        car.VModel = reader["VModel"].ToString();
                        car.VRegNo = reader["VRegNo"].ToString();
                        car.VRegDate = Convert.ToDateTime(reader["VRegDate"]);
                        cars.Add(car);
                    }

                    connection.Close();
                }
            }
            catch
            {

            }

            return cars;
        }

        public int Register(Car car)
        {
            int result = -1;

            string sql = "INSERT INTO CarPark (OFirstName, OLastName, OPhone, OUnit, OAppNo, VMake, VModel, VRegNo, VRegDate) ";
            sql += "VALUES('";
            sql += car.OFirstName;
            sql += "','" + car.OLastName;
            sql += "','" + car.OPhone;
            sql += "','" + car.OUnit;
            sql += "','" + car.OAppNo;
            sql += "','" + car.VMake;
            sql += "','" + car.VModel;
            sql += "','" + car.VRegNo;
            sql += "','" + DateTime.Today.ToString("d");
            sql += "');";

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch
            {
                result = 0;
            }
            
            return result;
        }

        public int Modify(Car car)
        {
            int result = -1;

            string sql = "UPDATE CarPark SET ";
            sql += "OFirstName = '" + car.OFirstName;
            sql += "', OLastName = '" + car.OLastName;
            sql += "', OPhone = '" + car.OPhone;
            sql += "', OUnit = '" + car.OUnit;
            sql += "', OAppNo = '" + car.OAppNo;
            sql += "', VMake = '" + car.VMake;
            sql += "', VModel = '" + car.VModel;
            sql += "', VRegNo = '" + car.VRegNo;
            sql += "' WHERE ID = '" + car.ID + "';";

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch
            {
                result = 0;
            }
            return result;
        }

        public int Delete(Car car)
        {
            int result = -1;
            string sql = "DELETE FROM CarPark ";
            sql += "WHERE ID = '" + car.ID + "';";

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch
            {
                result = 0;
            }
            return result;
        }

    }
}